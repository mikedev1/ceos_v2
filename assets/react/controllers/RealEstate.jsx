import * as React from "react";
import { ThemeProvider, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import MobileStepper from "@mui/material/MobileStepper";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import Link from "@mui/material/Link";


const AutoPlaySwipeableViews = autoPlay(SwipeableViews);



export default function RealEstate(props) {
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const images = [props.image];
    const maxSteps = images[0].length;
    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleStepChange = (step) => {
        setActiveStep(step);
    };
    return (
        <Box sx={{ maxWidth: 400, flexGrow: 1 ,}}>
            <Paper
                square
                elevation={0}
                className= "jean"
                sx={{
                    display: "flex",
                    alignItems: "center",
                    height: 50,
                    pl: 2,
                    bgcolor: "background.default",
                }}
            >
                <Grid container spacing={0}>
                    <Grid>
                        <Typography sx={{fontSize: 15, fontWeight: 'bold',alignContent: 'center'}}>Appartement {props.titre} {props.postalcode}</Typography>
                    </Grid>
                    <Grid>
                        <Typography sx={{color: 'success.dark',display: 'inline',fontWeight: 'bold',mx: 0.5,fontSize: 20, }}>{props.prix}€</Typography>
                    </Grid>
                </Grid>
            </Paper>
            <Link href={props.path}>
            <AutoPlaySwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={activeStep}
                onChangeIndex={handleStepChange}
                enableMouseEvents
                interval={14000}
            >
                {images[0].map((step, index) => (
                    <div key={step}>
                        {Math.abs(activeStep - index) <= 2 ? (
                        
                            <Box
                                component="img"
                                sx={{
                                    height: 255,
                                    display: "block",
                                    maxWidth: 400,
                                    overflow: "hidden",
                                    width: "100%",
                                }}
                                src={step}
                                alt={step}
                            />
                        ) : null}
                    </div>
                ))}
                </AutoPlaySwipeableViews>
                </Link>
            <MobileStepper
                className="papi"
                steps={maxSteps}
                position="static"
                activeStep={activeStep}
                nextButton={
                    <Button
                        size="small"
                        onClick={handleNext}
                        disabled={activeStep === maxSteps - 1}
                    >
                        Next
                        {theme.direction === "rtl" ? (
                            <KeyboardArrowLeft />
                        ) : (
                            <KeyboardArrowRight />
                        )}
                    </Button>
                }
                backButton={
                    <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                        {theme.direction === "rtl" ? (
                            <KeyboardArrowRight />
                        ) : (
                            <KeyboardArrowLeft />
                        )}
                        Back
                    </Button>
                }
            />
        </Box>
    );
}


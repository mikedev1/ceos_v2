<?php

namespace App\Service;

use Exception;
use App\Entity\RealEstateSalesman;
use App\Command\TakeDataFromXMLCommand;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\RealEstateSalesmanRepository;
use Doctrine\ORM\EntityManagerInterface;

class XmlDataRepositoryService
{
    public function __construct(
        RealEstateSalesmanRepository $realEstateRepository,
        EntityManagerInterface $entityManagerInterface
    ) {
        $this->realEstateRepository = $realEstateRepository;
        $this->entityManagerInterface = $entityManagerInterface;
    }

    public function getData(
        ManagerRegistry $doctrine,
        TakeDataFromXMLCommand $dataXmlCommand,
    ): Response {
        $datasXml = $dataXmlCommand->getData();
        $entityManagerInterface = $this->entityManagerInterface;
        $i = 0;
        foreach ($datasXml['bien'] as  $data) {

            $reference = $data['reference_a_afficher'];
            $city = $data['ville'];
            $adress = $data['adresse_mandataire'];
            $postalCode = $data['code_postal'];
            $roomNumber = $data['nb_piece'];
            $surface = $data['surface_habitable'];
            $price = $data['prix'];
            $bedroom = $data['nb_chambre'];
            $carPark = $data['stationnement_interne'];

            //select entity by reference
            $selectRealEstateSalesmanByReference = $entityManagerInterface->createQuery(
                '
                SELECT r
                FROM App\Entity\RealEstateSalesman r
                WHERE r.reference = :reference      
                '
            )->setParameter('reference', $reference);


            //Si la réference existe déjà
            if (!empty($selectRealEstateSalesmanByReference->getResult())) {
                $x =
                    $entityManagerInterface->createQuery(
                        '
                    UPDATE App\Entity\RealEstateSalesman r

                    SET 
                    r.city = :ville, 
                    r.postalCode = :code_postal,
                    r.adress = :adresse_mandataire,
                    r.roomNumber = :nb_piece, 
                    r.surface = :surface_habitable, 
                    r.price = :prix, 
                    r.bedroom = :nb_chambre, 
                    r.carPark = :stationnement_interne          
                    WHERE r.reference = :reference
                    '
                    )
                    ->setParameter('reference', $reference)
                    ->setParameter('ville', $city)
                    ->setParameter('adress', $adress)
                    ->setParameter('code_postal', $postalCode)
                    ->setParameter('nb_piece', $roomNumber)
                    ->setParameter('surface_habitable', $surface)
                    ->setParameter('prix', $price)
                    ->setParameter('nb_chambre', $bedroom)
                    ->setParameter('stationnement_interne', $carPark);

                foreach ($data['images'] as $image) {
                    $entityManagerInterface->createQuery(
                        '
                    UPDATE App\Entity\RealEstateSalesman r
                    SET 
                    r.image = :image, 
                    '
                    )

                        ->setParameter('image', $image);
                }




                //Si la réference n'existe pas onn crée une nouvelle instance de l'objet RealEstate
            } else {
                $realEstateSalesman = new RealEstateSalesman();
                $this->setData($realEstateSalesman, $data, $entityManagerInterface);
            }
        }
        //dd();
        return new Response("Sauvegarde de l'annonce en base de données");
    }

    private function setData($doctrineEntity, $data, $entityManager)
    {
        $doctrineEntity->setPostalCode($data['code_postal']);
        $doctrineEntity->setPrice($data['prix']);
        $doctrineEntity->setCity($data['ville']);
        $doctrineEntity->setAdress($data['adresse_mandataire']);
        $doctrineEntity->setRoomNumber($data['nb_piece']);
        $doctrineEntity->setSurface($data['surface_habitable']);
        $doctrineEntity->setFloor($data['etage']);
        $doctrineEntity->setBedroom($data['nb_chambre']);
        $doctrineEntity->setCarParkInternal($data['stationnement_interne']);
        $doctrineEntity->setReference($data['reference_a_afficher']);

        //injection des images dans un array en bdd
        foreach ($data['images']['image'] as $key => $image) {
            $arrayImage[] = $image['#'];
        }

        $doctrineEntity->setImage($arrayImage);
        $entityManager->persist($doctrineEntity);
        $entityManager->flush();
    }
}

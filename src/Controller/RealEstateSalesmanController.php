<?php

namespace App\Controller;

use App\Command\TakeDataFromXMLCommand;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\RealEstateSalesmanRepository;
use App\Service\XmlDataRepository;
use App\Service\XmlDataRepositoryService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RealEstateSalesmanController extends AbstractController
{
    private TakeDataFromXMLCommand $dataXml;

    public function __construct(
        TakeDataFromXMLCommand $dataXml,
        XmlDataRepositoryService $dataXmlRepo,
        ManagerRegistry $managerRegistry,
        RealEstateSalesmanRepository $realEstateSalesmanRepository
    ) {
        $this->dataXml = $dataXml;
        $this->dataXmlRepo = $dataXmlRepo;
        $this->managerRegistery = $managerRegistry;
        $this->realEstateSalesmanRepository = $realEstateSalesmanRepository;
    }

    #[Route('/real/estate/salesman', name: 'app_real_estate_salesman')]
    public function index(
        PaginatorInterface $paginator,
        Request $request,
    ): Response {
        $dataXmlRepo = $this->dataXmlRepo->getData(
            $this->managerRegistery,
            $this->dataXml
        );


        /*    // injection des données xml des publications
        $datas = $this->dataXml->getData();
        // dd($datas);
        $realEstateSalesman = $datas['bien'];
        foreach ($realEstateSalesman as $villes) {

            $ville[] = $villes['ville'];
            
        }
        $pagination = $paginator->paginate(
            $ville, // query NOT result
            $request->query->getInt('page', 1), //page number
            10 //limit per page
        );
        /* injection des données xml des publications */
        
        
        // $images = $villes['images']['image'];
        // $y = [];
        // // récuperation des url des images.
        // foreach ($images as $imgs) {

        //     $y[] = $imgs['#'];
        // }; 

        //Ajout des données sauvegardé en base de données

        $datas = $this->realEstateSalesmanRepository->findAll();
        //dd($datas);

        return $this->render('real_estate_salesman/index.html.twig', [

            'datas' => $datas

            /* 'realEstate' => $pagination,
            'datas' => $realEstateSalesman,
            'images' => $y */
        ]);
    }

    #[Route('/real/estate/salesman/search', name: 'app_real_estate_search', methods: ['GET'])]
    public function search(
        RealEstateSalesmanRepository $realEstateSalesmanRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $value = $request->query->get("search-value");
        // on cherche dans la requête une variable nommée search-value issue d'un name de formulaire
        //dd($value); dd = dump & die
        // recherche avec paginator
        $realEstateSalesman = $realEstateSalesmanRepository->searchForPaginator($value);
        $pagination = $paginator->paginate(
            $realEstateSalesman,
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('real_estate_salesman/index.html.twig', [
            'datas' => $pagination
        ]);
    }

    #[Route("/real/estate/salesman/detail/{reference}", name: 'app_real_estate_detail')]
    public function detail(RealEstateSalesmanRepository $realEstateSalesmanRepository, string $reference): Response
    {
        
        return $this->render('real_estate_salesman/detail.html.twig', [
            'realEstate' => $realEstateSalesmanRepository->find($reference),
            
        ]);
    }
}

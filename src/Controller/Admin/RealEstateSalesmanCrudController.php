<?php

namespace App\Controller\Admin;

use App\Form\RealEstateImageType;
use App\Entity\RealEstateCategory;
use App\Entity\RealEstateSalesman;
use Vich\UploaderBundle\Form\Type\VichFileType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RealEstateSalesmanCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RealEstateSalesman::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('adress'),
            IntegerField::new('postalCode'),
            TextField::new('city'),
            CollectionField::new(
                'imageRealEstates',
                'Image'
            )->setEntryType(
                RealEstateImageType::class
            )->setFormTypeOption('by_reference', false),
            CollectionField::new(
                'imageRealEstates',
                'Image'
            )->setTemplatePath('real_estate_salesman/images.html.twig')->onlyOnDetail(),
            IntegerField::new('roomNumber'),
            NumberField::new('surface'),
            MoneyField::new('price')->setCurrency('EUR'),
            IntegerField::new('bedroom'),
            IntegerField::new('floor'),
            BooleanField::new('carPark'),
            AssociationField::new('category', 'Type de bien')->renderAsNativeWidget()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::SAVE_AND_ADD_ANOTHER);
    }
}

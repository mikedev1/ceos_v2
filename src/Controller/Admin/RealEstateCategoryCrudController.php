<?php

namespace App\Controller\Admin;

use App\Entity\RealEstateCategory;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class RealEstateCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return RealEstateCategory::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
        ];
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Controller\Admin\UserCrudController;
use App\Entity\RealEstateCategory;
use App\Entity\RealEstateSalesman;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use Symfony\Component\Security\Core\User\UserInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        /* $url = $this->adminUrlGenerator
            ->setController(UserCrudController::class)
            ->generateUrl(); */

        //return $this->redirect($url);

        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        //  return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('CEOS')
            ->setFaviconPath('favicon/logo_ceos.svg');
    }

    public function configureMenuItems(): iterable
    {
        /** @var \App\Entity\User $user */
        $user = $this->getUser();

        yield MenuItem::linkToRoute('Back To CEOS', 'fa fa-globe', 'app_home_index');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        if (in_array("ROLE_ADMIN", $user->getRoles())) {
            yield MenuItem::subMenu('Users', 'fas fa-users')
                ->setSubItems([
                    MenuItem::linkToCrud('Create User', 'fas fa-plus', User::class)
                        ->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('show / edit User', 'fas fa-eye', User::class)
                ]);
            yield MenuItem::subMenu('Vente immobiliere', 'fas fa-users')
                ->setSubItems([
                    MenuItem::linkToCrud('Ajouter une vente', 'fas fa-plus', RealEstateSalesman::class)
                        ->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('show / edit', 'fas fa-eye', RealEstateSalesman::class)
                ]);
            yield MenuItem::subMenu('Type de bien', 'fas fa-users')
                ->setSubItems([
                    MenuItem::linkToCrud('Ajouter un type', 'fas fa-plus', RealEstateCategory::class)
                        ->setAction(Crud::PAGE_NEW),
                    MenuItem::linkToCrud('show / edit', 'fas fa-eye', RealEstateCategory::class)
                ]);
        } else {
            yield MenuItem::subMenu('Users', 'fas fa-users')
                ->setSubItems([
                    MenuItem::linkToCrud('show User', 'fas fa-eye', User::class)
                ]);
        }
    }
}

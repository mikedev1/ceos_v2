<?php

namespace App\Form;

use App\Entity\ImageRealEstate;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class RealEstateImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('imageFile', VichFileType::class, [
                "label" => "Images du bien"
            ])
            ->add('active', null, [
                "label" => "Cochez si vous voulez l'utilser comme vignette de l'annonce"
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ImageRealEstate::class,
        ]);
    }
}

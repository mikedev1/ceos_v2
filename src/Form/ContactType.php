<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                "label" => "Votre nom",
                "attr" => [
                    "placeholder" => "Dupont"
                ]
            ])
            ->add('prenom', TextType::class, [
                "label" => "Votre prénom",
                "attr" => [
                    "placeholder" => "Alain"
                ]
            ])
            ->add('email', EmailType::class, [
                "label" => "Votre adresse mail",
                "attr" => [
                    "placeholder" => "alain.dupont@mail.fr"
                ]
            ])
            ->add('sujet')
            ->add('message', TextareaType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}

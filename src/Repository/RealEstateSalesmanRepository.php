<?php

namespace App\Repository;

use App\Entity\RealEstateSalesman;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RealEstateSalesman>
 *
 * @method RealEstateSalesman|null find($id, $lockMode = null, $lockVersion = null)
 * @method RealEstateSalesman|null findOneBy(array $criteria, array $orderBy = null)
 * @method RealEstateSalesman[]    findAll()
 * @method RealEstateSalesman[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RealEstateSalesmanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RealEstateSalesman::class);
    }

    public function add(RealEstateSalesman $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(RealEstateSalesman $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * permet d'effectuer une recherche en utilsant Paginator
     *
     * @param [type] $value
     * @return void
     */
    public function searchForPaginator($value) // pour use le paginator
    {
        return $this->createQueryBuilder('l')
            ->where('l.city LIKE :val')
            ->setParameter('val', '%' . $value . '%');
    }

//    /**
//     * @return RealEstateSalesman[] Returns an array of RealEstateSalesman objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RealEstateSalesman
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

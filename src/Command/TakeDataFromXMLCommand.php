<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

#[AsCommand(
    name: 'app:xml-data',
    description: 'Récupération des données XML',
)]
class TakeDataFromXMLCommand extends Command
{

    private $dataDirectory;

    public function __construct(
        $dataDirectory
    ) {
        $this->dataDirectory = $dataDirectory;
        parent::__construct();
    }
    protected function configure(): void
    {
        $this            /* name || shortcut || mode || description */
            -> setHelp("Injection des données du fichier xml contenant les publications immobilieres")
            ->addArgument('service', InputArgument::OPTIONAL, 'Nom du service')
            ->addOption('option1', '-i', InputOption::VALUE_NONE, 'Option description')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->getData();
        $io->title("Injection des donnés xml");


        $io->success("Votre demande a bien été validée");

        /* $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.'); */

        return Command::SUCCESS;
    }

    public function getData()
    {
        $file = $this->dataDirectory . "natty.xml";

        //dd($file);
        /**
         * return xml
         */
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);

        $normalizers = [new ObjectNormalizer()];

        $encoders = [
            new XmlEncoder()
        ];

        $serializer = new Serializer($normalizers, $encoders);
        /**
         * @var string $fileString
         */
        $fileString = file_get_contents($file);

        //dd($fileString);

        $data = $serializer->decode($fileString, $fileExtension);

        //dd($data);

        return $data;
    }
}

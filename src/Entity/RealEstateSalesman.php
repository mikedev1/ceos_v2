<?php

namespace App\Entity;

use Serializable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use App\Repository\RealEstateSalesmanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: RealEstateSalesmanRepository::class)]
#[UniqueEntity('reference')]
class RealEstateSalesman
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $adress;

    #[ORM\Column(type: 'string', length: 255)]
    private $city;

    #[ORM\Column(type: 'integer')]
    private $postalCode;

    #[ORM\Column(type: 'integer')]
    private $roomNumber;

    #[ORM\Column(type: 'float')]
    private $surface;

    #[ORM\Column(type: 'float')]
    private $price;

    #[ORM\Column(type: 'integer')]
    private $bedroom;

    #[ORM\Column(type: 'string', nullable: true)]
    private $floor;

    #[ORM\Column(type: 'boolean')]
    private $carPark;

    #[ORM\ManyToOne(targetEntity: RealEstateCategory::class, inversedBy: 'realEstateSalesmen')]
    private $category;

    #[ORM\Column(type: 'array', nullable: true)]
    private $image = [];

    #[ORM\Column(type: 'string', length: 70, unique: true)]
    private $reference;

    public function __construct()
    {
        $this->realEstateCategories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getRoomNumber(): ?int
    {
        return $this->roomNumber;
    }

    public function setRoomNumber(int $roomNumber): self
    {
        $this->roomNumber = $roomNumber;

        return $this;
    }

    public function getSurface(): ?float
    {
        return $this->surface;
    }

    public function setSurface(float $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getBedroom(): ?int
    {
        return $this->bedroom;
    }

    public function setBedroom(int $bedroom): self
    {
        $this->bedroom = $bedroom;

        return $this;
    }

    public function getFloor(): ?string
    {
        return $this->floor;
    }

    public function setFloor(string $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    public function isCarParkInternal(): ?bool
    {
        return $this->carPark;
    }

    public function setCarParkInternal(bool $carPark): self
    {
        $this->carPark = $carPark;

        return $this;
    }

    public function getCategory(): ?RealEstateCategory
    {
        return $this->category;
    }

    public function setCategory(?RealEstateCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    // public function serialize()
    // {

    //     $this->imageFile = base64_encode($this->imageFile);
    // }

    // public function unserialize($data)
    // {

    //     $this->imageFile = base64_decode($data);
    // }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getImage(): ?array
    {
        return $this->image;
    }

    public function setImage(?array $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\RealEstateCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RealEstateCategoryRepository::class)]
class RealEstateCategory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: RealEstateSalesman::class)]
    private $realEstateSalesmen;

    public function __construct()
    {
        $this->realEstateSalesmen = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, RealEstateSalesman>
     */
    public function getRealEstateSalesmen(): Collection
    {
        return $this->realEstateSalesmen;
    }

    public function addRealEstateSalesman(RealEstateSalesman $realEstateSalesman): self
    {
        if (!$this->realEstateSalesmen->contains($realEstateSalesman)) {
            $this->realEstateSalesmen[] = $realEstateSalesman;
            $realEstateSalesman->setCategory($this);
        }

        return $this;
    }

    public function removeRealEstateSalesman(RealEstateSalesman $realEstateSalesman): self
    {
        if ($this->realEstateSalesmen->removeElement($realEstateSalesman)) {
            // set the owning side to null (unless already changed)
            if ($realEstateSalesman->getCategory() === $this) {
                $realEstateSalesman->setCategory(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return (string)$this->getName();
    }
}
